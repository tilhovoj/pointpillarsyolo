import random
import numpy as np
import torch
from torch.utils.data import DataLoader, WeightedRandomSampler
from functools import partial


def collate_fn(list_data):
    batched_pts_list, batched_gt_bboxes_list = [], []
    batched_labels_list, batched_names_list = [], []
    batched_difficulty_list = []
    batched_img_list, batched_calib_list = [], []
    for data_dict in list_data:
        pts, gt_bboxes_3d = data_dict['pts'], data_dict['gt_bboxes_3d']
        gt_labels, gt_names = data_dict['gt_labels'], data_dict['gt_names']
        difficulty = data_dict['difficulty']
        image_info, calbi_info = data_dict['image_info'], data_dict['calib_info']

        batched_pts_list.append(torch.from_numpy(pts))
        batched_gt_bboxes_list.append(torch.from_numpy(gt_bboxes_3d))
        batched_labels_list.append(torch.from_numpy(gt_labels))
        batched_names_list.append(gt_names) # List(str)
        batched_difficulty_list.append(torch.from_numpy(difficulty))
        batched_img_list.append(image_info)
        batched_calib_list.append(calbi_info)
    
    rt_data_dict = dict(
        batched_pts=batched_pts_list,
        batched_gt_bboxes=batched_gt_bboxes_list,
        batched_labels=batched_labels_list,
        batched_names=batched_names_list,
        batched_difficulty=batched_difficulty_list,
        batched_img_info=batched_img_list,
        batched_calib_info=batched_calib_list
    )

    return rt_data_dict


def get_dataloader(dataset, batch_size, num_workers, randomize=True, drop_last=False, husky_weight=None):
    collate = collate_fn
    return DataLoader(
        dataset=dataset,
        batch_size=batch_size,
        shuffle=randomize,
        num_workers=num_workers,
        drop_last=drop_last, 
        collate_fn=collate,
    )

    if randomize:
        if husky_weight is None:
            husky_weight = 0.5

        num_husky = 0
        num_kitti = 0
        for sample in dataset:
            if sample['image_info']['image_idx'][0] == 'h':
                num_husky += 1
            else:
                num_kitti += 1

        print(f'Weight is {husky_weight}')
        print(f'Husky samples have weight {husky_weight / num_husky}\nKitti samples have weight {husky_weight / num_kitti}')
        sample_weights = np.zeros(len(dataset))
        for i, sample in enumerate(dataset):
            if sample['image_info']['image_idx'][0] == 'h':
                sample_weights[i] = husky_weight / num_husky
            else:
                sample_weights[i] = (1.0 - husky_weight) / num_kitti

        sampler = WeightedRandomSampler(
            sample_weights,
            len(sample_weights)
        )
        dataloader = DataLoader(
            dataset=dataset,
            batch_size=batch_size,
            sampler=sampler,
            num_workers=num_workers,
            drop_last=drop_last, 
            collate_fn=collate,
        )
    else:
        dataloader = DataLoader(
            dataset=dataset,
            batch_size=batch_size,
            shuffle=False,
            num_workers=num_workers,
            drop_last=drop_last, 
            collate_fn=collate,
        )
    return dataloader
