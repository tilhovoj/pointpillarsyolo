import argparse
import cv2
import os

def read_label(path):
    if not os.path.exists(path):
        return []
    
    with open(path, 'rt') as label_file:
        return [[float(x) if i != 0 else int(x) for i, x in enumerate(line.split(' '))] for line in label_file.readlines()]

def visualize_detections(images_root, labels_root, saved_root):
    for id in os.listdir(images_root):
        id = id[:-4]
        img_path = os.path.join(images_root, f'{id}.png')
        label_path = os.path.join(labels_root, f'{id}.txt')

        img = cv2.imread(img_path)

        labels = read_label(label_path)

        colors = [[0, 0, 255], [0, 255, 0]]
        
        img_height, img_width, _ = img.shape

        ## 1. visualize 2d 
        for label in labels:
            x_center = label[1]
            y_center = label[2]
            width = label[3]
            height = label[4]

            conf = label[5]
            x1 = int((x_center - width / 2) * img_width)
            x2 = int((x_center + width / 2) * img_width)
            y1 = int((y_center - height / 2) * img_height)
            y2 = int((y_center + height / 2) * img_height)
            cv2.rectangle(img, (x1, y1), (x2, y2), colors[label[0]], 2)
        
        out_path = os.path.join(saved_root, f'{id}.png')
        print(f'Saving to \'{out_path}\'')
        cv2.imwrite(out_path, img)
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Visualize bounding boxes obtained by YOLOv5')
    parser.add_argument('--images_root', default='/home/robot/kn_experiment/training/image_2')
    parser.add_argument('--labels_root', default='/home/robot/Desktop/PointPillarsYOLO/misc/labels')
    parser.add_argument('--saved_root', default='tmp')
    args = parser.parse_args()
    
    images_root = args.images_root
    labels_root = args.labels_root
    saved_root = args.saved_root
    os.makedirs(saved_root, exist_ok=True)
    visualize_detections(images_root, labels_root, saved_root)
