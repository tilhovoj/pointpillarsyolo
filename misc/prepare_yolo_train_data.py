import os
import shutil

with open('../dataset/ImageSets_KITTI/train.txt', 'r') as train_file:
    train_sample_ids = [x.strip() for x in train_file.readlines()]

with open('../dataset/ImageSets_KITTI/val.txt', 'r') as val_file:
    val_sample_ids = [x.strip() for x in val_file.readlines()]

KITTI_ROOT = '/home/robot/training'
YOLO_ROOT = '/home/robot/Desktop/yolov5_custom_dataset'

for id in train_sample_ids:
    src_img_path = os.path.join(KITTI_ROOT, 'image_2', id + '.png')
    dst_img_path = os.path.join(YOLO_ROOT, 'images/train', id + '.png')
    shutil.copyfile(src_img_path, dst_img_path)

    src_lbl_path = os.path.join(KITTI_ROOT, 'image_label', id + '.txt')
    dst_lbl_path = os.path.join(YOLO_ROOT, 'labels/train', id + '.txt')
    shutil.copyfile(src_lbl_path, dst_lbl_path)

for id in val_sample_ids:
    src_img_path = os.path.join(KITTI_ROOT, 'image_2', id + '.png')
    dst_img_path = os.path.join(YOLO_ROOT, 'images/val', id + '.png')
    shutil.copyfile(src_img_path, dst_img_path)

    src_lbl_path = os.path.join(KITTI_ROOT, 'image_label', id + '.txt')
    dst_lbl_path = os.path.join(YOLO_ROOT, 'labels/val', id + '.txt')
    shutil.copyfile(src_lbl_path, dst_lbl_path)
