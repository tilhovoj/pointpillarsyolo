import argparse
import cv2
import numpy as np
import os

def read_points(file_path, dim=4):
    suffix = os.path.splitext(file_path)[1] 
    assert suffix in ['.bin', '.ply']
    if suffix == '.bin':
        return np.fromfile(file_path, dtype=np.float32).reshape(-1, dim)
    else:
        raise NotImplementedError

def read_label(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    lines = [line.strip().split(' ') for line in lines]
    annotation = {}
    annotation['name'] = np.array([line[0] for line in lines])
    annotation['truncated'] = np.array([line[1] for line in lines], dtype=np.float)
    annotation['occluded'] = np.array([line[2] for line in lines], dtype=np.int)
    annotation['alpha'] = np.array([line[3] for line in lines], dtype=np.float)
    annotation['bbox'] = np.array([line[4:8] for line in lines], dtype=np.float)
    annotation['dimensions'] = np.array([line[8:11] for line in lines], dtype=np.float)[:, [2, 0, 1]] # hwl -> camera coordinates (lhw)
    annotation['location'] = np.array([line[11:14] for line in lines], dtype=np.float)
    annotation['rotation_y'] = np.array([line[14] for line in lines], dtype=np.float)
    
    return annotation

def make_labels(root, labels_out_root):
    for id in os.listdir(os.path.join(root, 'image_2')):
        id = id[:-4]
        img_path = os.path.join(root, 'image_2', f'{id}.png')
        label_path = os.path.join(root, 'label_2', f'{id}.txt')
        
        img = cv2.imread(img_path)
        annotation_dict = read_label(label_path)

        bboxes = annotation_dict['bbox']
        names = annotation_dict['name']

        classes = {
            'DontCare': -1,
            'Misc': -1,
            'Tram': -1,
            'Person_sitting': -1,
            'Truck': 0,
            'Car': 0,
            'Van': 0,
            'Cyclist': 1,
            'Pedestrian': 2, 
        }

        img_height, img_width, _ = img.shape

        objects = []
        for i, bbox in enumerate(bboxes):
            label_class = classes[names[i]]

            # Ignored class
            if label_class < 0:
                continue
            
            x1, y1, x2, y2 = bbox
            x_center = (x1 + x2) / 2
            y_center = (y1 + y2) / 2
            width = x2 - x1
            height = y2 - y1
            x_center, y_center = x_center / img_width, y_center / img_height
            width, height = width / img_width, height / img_height
            line = ' '.join([str(label_class), str(x_center), str(y_center), str(width), str(height)]) + '\n'
            objects.append(line)
        
        with open(os.path.join(labels_out_root, f'{id}.txt'), 'w') as out_label_file:
            out_label_file.writelines(objects)



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Make KITTI labels for 2D')
    parser.add_argument('--data_root', default='/home/robot/training')
    parser.add_argument('--labels_out', default='/home/robot/training/image_label')
    args = parser.parse_args()
    
    root = args.data_root
    labels_out = args.labels_out
    os.makedirs(labels_out, exist_ok=True)
    make_labels(root, labels_out)
