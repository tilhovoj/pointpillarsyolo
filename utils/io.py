import numpy as np
import cv2
import os
import pickle

from .process import projection_matrix_to_CRT_kitti, points_camera2lidar, get_frustum, group_rectangle_vertexs, group_plane_equation, points_in_bboxes


def read_pickle(file_path, suffix='.pkl'):
    assert os.path.splitext(file_path)[1] == suffix
    with open(file_path, 'rb') as f:
        data = pickle.load(f)
    return data


def write_pickle(results, file_path):
    with open(file_path, 'wb') as f:
        pickle.dump(results, f)


def read_yolo_label(path: str):
    if not os.path.exists(path):
        return []
    
    with open(path, 'rt') as label_file:
        return [[float(x) if i != 0 else int(x) for i, x in enumerate(line.split(' '))] for line in label_file.readlines()]

###

# Modified from https://github.com/open-mmlab/mmdetection3d/blob/f45977008a52baaf97640a0e9b2bbe5ea1c4be34/mmdet3d/core/bbox/box_np_ops.py#L609
def points_inside_2d_box_mask(points, r0_rect, tr_velo_to_cam, P2, x1, y1, x2, y2):
    """Get boolean mask of points which are inside of bounding box in image pixel coordinates.
    Args:
        points (np.ndarray, shape=[N, 3+dims]): Total points.
        rect (np.ndarray, shape=[4, 4]): Matrix to project points in
            specific camera coordinate (e.g. CAM2) to CAM0.
        Trv2c (np.ndarray, shape=[4, 4]): Matrix to project points in
            camera coordinate to lidar coordinate.
        P2 (p.array, shape=[4, 4]): Intrinsics of Camera2.
        image_shape (list[int]): Shape of image.
    """
    # 5x faster than remove_outside_points_v1(2ms vs 10ms)
    C, R, T = projection_matrix_to_CRT_kitti(P2)
    image_bbox = [x1, y1, x2, y2]
    frustum = get_frustum(image_bbox, C)
    frustum -= T
    frustum = np.linalg.inv(R) @ frustum.T
    frustum = points_camera2lidar(frustum.T[None, ...], tr_velo_to_cam, r0_rect) # (1, 8, 3)
    group_rectangle_vertexs_v = group_rectangle_vertexs(frustum)
    frustum_surfaces = group_plane_equation(group_rectangle_vertexs_v)
    indices = points_in_bboxes(points[:, :3], frustum_surfaces) # (N, 1)
    return indices.reshape([-1])

###

def read_points(file_path, dim=4):
    # Point clouds in 'velodyne_reduced' and 'gt_database' directories have pressumably already been decorated
    already_decorated_with_yolo = 'reduced' in file_path or 'gt_database' in file_path

    suffix = os.path.splitext(file_path)[1]
    num_decor_dims = 1
    
    assert suffix in ['.bin', '.ply']
    if suffix == '.bin':
        if already_decorated_with_yolo:
            dim += num_decor_dims
        points = np.fromfile(file_path, dtype=np.float32).reshape(-1, dim)

        if not already_decorated_with_yolo:
            dir_name, base_name = os.path.split(file_path)
            head_base_name = base_name[:-4]
            
            image_path = os.path.join(os.path.dirname(dir_name), 'image_2', f'{head_base_name}.png')
            label_path = os.path.join(os.path.dirname(dir_name), 'yolo_labels', f'{head_base_name}.txt')
            calib_path = os.path.join(os.path.dirname(dir_name), 'calib', f'{head_base_name}.txt')
            
            calib = read_calib(calib_path)
            labels = read_yolo_label(label_path)
            img_height, img_width, _ = cv2.imread(image_path).shape

            R0_rect = calib['R0_rect'] # 4x4
            Tr_velo_to_cam = calib['Tr_velo_to_cam'] # 4x4
            P2 = calib['P2'] # 4x4

            decorations = np.zeros((points.shape[0], num_decor_dims), dtype=np.float32)

            for label in labels:
                label_class = label[0]
                x_center = label[1]
                y_center = label[2]
                width = label[3]
                height = label[4]

                conf = label[5]
                x1 = (x_center - width / 2) * img_width
                x2 = (x_center + width / 2) * img_width
                y1 = (y_center - height / 2) * img_height
                y2 = (y_center + height / 2) * img_height

                inside_box_mask = points_inside_2d_box_mask(points, R0_rect, Tr_velo_to_cam, P2, x1, y1, x2, y2)
                decorations[inside_box_mask, label_class] += conf
            
            points = np.hstack([points, decorations])
        
        return points
    else:
        raise NotImplementedError


def write_points(lidar_points, file_path):
    suffix = os.path.splitext(file_path)[1] 
    assert suffix in ['.bin', '.ply']
    if suffix == '.bin':
        with open(file_path, 'w') as f:
            lidar_points.tofile(f)
    else:
        raise NotImplementedError


def read_calib(file_path, extend_matrix=True):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    lines = [line.strip() for line in lines]
    P0 = np.array([item for item in lines[0].split(' ')[1:]], dtype=np.float).reshape(3, 4)
    P1 = np.array([item for item in lines[1].split(' ')[1:]], dtype=np.float).reshape(3, 4)
    P2 = np.array([item for item in lines[2].split(' ')[1:]], dtype=np.float).reshape(3, 4)
    P3 = np.array([item for item in lines[3].split(' ')[1:]], dtype=np.float).reshape(3, 4)

    R0_rect = np.array([item for item in lines[4].split(' ')[1:]], dtype=np.float).reshape(3, 3)
    Tr_velo_to_cam = np.array([item for item in lines[5].split(' ')[1:]], dtype=np.float).reshape(3, 4)
    Tr_imu_to_velo = np.array([item for item in lines[6].split(' ')[1:]], dtype=np.float).reshape(3, 4)

    if extend_matrix:
        P0 = np.concatenate([P0, np.array([[0, 0, 0, 1]])], axis=0)
        P1 = np.concatenate([P1, np.array([[0, 0, 0, 1]])], axis=0)
        P2 = np.concatenate([P2, np.array([[0, 0, 0, 1]])], axis=0)
        P3 = np.concatenate([P3, np.array([[0, 0, 0, 1]])], axis=0)

        R0_rect_extend = np.eye(4, dtype=R0_rect.dtype)
        R0_rect_extend[:3, :3] = R0_rect
        R0_rect = R0_rect_extend

        Tr_velo_to_cam = np.concatenate([Tr_velo_to_cam, np.array([[0, 0, 0, 1]])], axis=0)
        Tr_imu_to_velo = np.concatenate([Tr_imu_to_velo, np.array([[0, 0, 0, 1]])], axis=0)

    calib_dict=dict(
        P0=P0,
        P1=P1,
        P2=P2,
        P3=P3,
        R0_rect=R0_rect,
        Tr_velo_to_cam=Tr_velo_to_cam,
        Tr_imu_to_velo=Tr_imu_to_velo
    )
    return calib_dict


def read_label(file_path):
    with open(file_path, 'r') as f:
        lines = f.readlines()
    lines = [line.strip().split(' ') for line in lines]
    annotation = {}
    annotation['name'] = np.array([line[0] for line in lines])
    annotation['truncated'] = np.array([line[1] for line in lines], dtype=np.float)
    annotation['occluded'] = np.array([line[2] for line in lines], dtype=np.int)
    annotation['alpha'] = np.array([line[3] for line in lines], dtype=np.float)
    annotation['bbox'] = np.array([line[4:8] for line in lines], dtype=np.float)
    try:
        annotation['dimensions'] = np.array([line[8:11] for line in lines], dtype=np.float)[:, [2, 0, 1]] # hwl -> camera coordinates (lhw)
    except:
        print(file_path)
        raise
    annotation['location'] = np.array([line[11:14] for line in lines], dtype=np.float)
    annotation['rotation_y'] = np.array([line[14] for line in lines], dtype=np.float)
    
    return annotation


def write_label(result, file_path, suffix='.txt'):
    '''
    result: dict,
    file_path: str
    '''
    assert os.path.splitext(file_path)[1] == suffix
    name, truncated, occluded, alpha, bbox, dimensions, location, rotation_y, score = \
        result['name'], result['truncated'], result['occluded'], result['alpha'], \
        result['bbox'], result['dimensions'], result['location'], result['rotation_y'], \
        result['score']
    
    with open(file_path, 'w') as f:
        for i in range(len(name)):
            bbox_str = ' '.join(map(str, bbox[i]))
            hwl = ' '.join(map(str, [dimensions[i][1], dimensions[i][2], dimensions[i][0]])) # camera lhw -> hwl
            xyz = ' '.join(map(str, location[i]))
            line = f'{name[i]} {truncated[i]} {occluded[i]} {alpha[i]} {bbox_str} {hwl} {xyz} {rotation_y[i]} {score[i]}\n'
            f.writelines(line)
