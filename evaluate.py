from scipy.optimize import linear_sum_assignment
import argparse
import numpy as np
import os
import torch
import pdb
from tqdm import tqdm

from utils import setup_seed, keep_bbox_from_image_range, \
    keep_bbox_from_lidar_range, write_pickle, write_label, \
    iou2d, iou3d_camera, iou_bev
from dataset import Kitti, get_dataloader
from model import PointPillars

EVAL_TRACKING = False

def get_timestamp_from_id(frame_id):
    return float(frame_id[frame_id.rfind('_') + 1:])

def do_tracking(det_results, CLASSES, saved_path):
    IOU_THRESHOLD = 0.1

    ids = sorted(det_results.keys())
    submit_folder = os.path.join(saved_path, 'submit')

    max_id = -1
    def generate_id():
        nonlocal max_id
        max_id += 1
        return max_id

    def generate_ids(count):
        nonlocal max_id
        max_id += 1
        ids = list(range(max_id, max_id + count))
        max_id += count - 1
        return ids

    # Initialize track ids for the first frame
    track_id_dict = {}
    track_id_dict[ids[0]] = generate_ids(len(det_results[ids[0]]['name']))

    velocity_dict = {}
    velocity_dict[ids[0]] = [[0.0, 0.0, 0.0]] * len(det_results[ids[0]]['name'])

    # Continue tracks, or initiilize new ones
    for prev_id, curr_id in zip(ids[:-1], ids[1:]):
        prev_t = get_timestamp_from_id(prev_id)
        curr_t = get_timestamp_from_id(curr_id)
        dt = curr_t - prev_t

        prev_track_ids = track_id_dict[prev_id]

        prev_result = det_results[prev_id]
        curr_result = det_results[curr_id]

        prev_count = len(prev_result['name'])
        curr_count = len(curr_result['name'])

        if prev_count == 0:
            track_id_dict[curr_id] = generate_ids(curr_count)
            continue
        if curr_count == 0:
            track_id_dict[curr_id] = []
            continue

        prev_location = prev_result['location'].astype(np.float32)
        prev_dimensions = prev_result['dimensions'].astype(np.float32)
        prev_rotation_y = prev_result['rotation_y'].astype(np.float32)
        curr_location = curr_result['location'].astype(np.float32)
        curr_dimensions = curr_result['dimensions'].astype(np.float32)
        curr_rotation_y = curr_result['rotation_y'].astype(np.float32)

        prev_bboxes3d = np.concatenate([prev_location.reshape(-1, 3), prev_dimensions.reshape(-1, 3), prev_rotation_y[:, None]], axis=-1)
        curr_bboxes3d = np.concatenate([curr_location.reshape(-1, 3), curr_dimensions.reshape(-1, 3), curr_rotation_y[:, None]], axis=-1)
        iou3d_v = iou3d_camera(torch.from_numpy(prev_bboxes3d.reshape(-1, 7)).cuda(), torch.from_numpy(curr_bboxes3d.reshape(-1, 7)).cuda())
        # contains (num_prev, num_curr) IoUs (everyone with everyone)
        ious = iou3d_v.cpu().numpy()
        # NOTE: scipy solves minimization problem, so negate IoUs
        prev_ind, curr_ind = linear_sum_assignment(-ious)

        # Initialize current track id list as -1
        curr_track_ids = -np.ones(curr_count, dtype=np.int32)
        curr_velocities = np.zeros((curr_count, 3), dtype=np.float32)

        # For every match which surpasses threshold, continue the track from previous frame
        # If it's below the threshold, generate new track
        for prev_match, curr_match in zip(prev_ind, curr_ind):
            if ious[prev_match, curr_match] < IOU_THRESHOLD:
                track_id = generate_id()
            else:
                track_id = prev_track_ids[prev_match]
            curr_track_ids[curr_match] = track_id

            prev_x, prev_y, prev_z = prev_location.reshape(-1, 3)[prev_match]
            curr_x, curr_y, curr_z = curr_location.reshape(-1, 3)[curr_match]
            dx, dy, dz = curr_x - prev_x, curr_y - prev_y, curr_z - prev_z
            vx, vy, vz = dx / dt, dy / dt, dz / dt
            curr_velocities[curr_match] = [vx, vy, vz]

        # Generate new track id for all unmatched detections
        unmatched_mask = curr_track_ids < 0
        curr_track_ids[unmatched_mask] = generate_ids(np.sum(unmatched_mask))

        curr_velocities[unmatched_mask] = np.zeros((np.sum(unmatched_mask), 3), dtype=np.float32)
        
        # Add the track ids to the dict
        track_id_dict[curr_id] = list(curr_track_ids)
        velocity_dict[curr_id] = list(curr_velocities)
    
    # Save out the track ids into the submit folder with detections as an extra field
    for frame_id, track_ids in track_id_dict.items():
        file_name = os.path.join(submit_folder, f'{frame_id}.txt')
        with open(file_name, 'rt') as f:
            lines = f.readlines()

        new_contents = []
        for line, track_id in zip(lines, track_ids):
            stripped = line[:-1]
            new_contents.append(f'{stripped} {track_id}\n')

        with open(file_name, 'wt') as f:
            f.writelines(new_contents)

    # Save out the estimated velocities
    for frame_id, velocities in velocity_dict.items():
        file_name = os.path.join(submit_folder, f'{frame_id}.txt')
        with open(file_name, 'rt') as f:
            lines = f.readlines()

        new_contents = []
        for line, velocity in zip(lines, velocities):
            stripped = line[:-1]
            vx, vy, vz = velocity
            new_contents.append(f'{stripped} {vx} {vy} {vz}\n')

        with open(file_name, 'wt') as f:
            f.writelines(new_contents)

def get_score_thresholds(tp_scores, total_num_valid_gt, num_sample_pts=41):
    score_thresholds = []
    tp_scores = sorted(tp_scores)[::-1]
    cur_recall, pts_ind = 0, 0
    for i, score in enumerate(tp_scores):
        lrecall = (i + 1) / total_num_valid_gt
        rrecall = (i + 2) / total_num_valid_gt

        if i == len(tp_scores) - 1:
            score_thresholds.append(score)
            break

        if (lrecall + rrecall) / 2 < cur_recall:
            continue

        score_thresholds.append(score)
        pts_ind += 1
        cur_recall = pts_ind / (num_sample_pts - 1)
    return score_thresholds


def write_out_prs(file_path, precisions, recalls):
    with open(file_path, 'wt') as f:
        for (p, r) in zip(precisions, recalls):
            f.write(f'{r} {p}\n')


def do_eval(det_results, gt_results, CLASSES, saved_path):
    '''
    det_results: list,
    gt_results: dict(id -> det_results)
    CLASSES: dict
    '''
    assert len(det_results) == len(gt_results)
    f = open(os.path.join(saved_path, 'eval_results.txt'), 'w')

    # 1. calculate iou
    ious = {
        'bbox_2d': [],
        'bbox_bev': [],
        'bbox_3d': []
    }
    ids = list(sorted(gt_results.keys()))
    for id in ids:
        gt_result = gt_results[id]['annos']
        det_result = det_results[id]

        # 1.1, 2d bboxes iou
        gt_bboxes2d = gt_result['bbox'].astype(np.float32)
        det_bboxes2d = det_result['bbox'].astype(np.float32)
        iou2d_v = iou2d(torch.from_numpy(gt_bboxes2d).reshape(-1, 4), torch.from_numpy(det_bboxes2d).reshape(-1, 4))
        ious['bbox_2d'].append(iou2d_v.cpu().numpy())

        # 1.2, bev iou
        gt_location = gt_result['location'].astype(np.float32)
        gt_dimensions = gt_result['dimensions'].astype(np.float32)
        gt_rotation_y = gt_result['rotation_y'].astype(np.float32)
        det_location = det_result['location'].astype(np.float32)
        det_dimensions = det_result['dimensions'].astype(np.float32)
        det_rotation_y = det_result['rotation_y'].astype(np.float32)

        gt_bev = np.concatenate([gt_location[:, [0, 2]], gt_dimensions[:, [0, 2]], gt_rotation_y[:, None]], axis=-1)
        det_bev = np.concatenate([det_location.reshape(-1, 3)[:, [0, 2]], det_dimensions.reshape(-1, 3)[:, [0, 2]], det_rotation_y[:, None]], axis=-1)
        iou_bev_v = torch.from_numpy(np.zeros((gt_bev.shape[0], det_bev.shape[0]))) # iou_bev(torch.from_numpy(gt_bev), torch.from_numpy(det_bev))
        ious['bbox_bev'].append(iou_bev_v.cpu().numpy())

        # 1.3, 3dbboxes iou
        gt_bboxes3d = np.concatenate([gt_location, gt_dimensions, gt_rotation_y[:, None]], axis=-1)
        det_bboxes3d = np.concatenate([det_location.reshape(-1, 3), det_dimensions.reshape(-1, 3), det_rotation_y[:, None]], axis=-1)
        iou3d_v = iou3d_camera(torch.from_numpy(gt_bboxes3d).cuda(), torch.from_numpy(det_bboxes3d.reshape(-1, 7)).cuda())
        ious['bbox_3d'].append(iou3d_v.cpu().numpy())

    MIN_IOUS = {
        'Pedestrian': [0.5, 0.5, 0.5],
        'Cyclist': [0.5, 0.5, 0.5],
        'Car': [0.7, 0.7, 0.5]
    }
    MIN_HEIGHT = [40, 25, 25]
    MIN_NUM_POINTS_THRESHOLD = 10

    overall_results = {}
    for e_ind, eval_type in enumerate(['bbox_2d', 'bbox_bev', 'bbox_3d']):
        eval_ious = ious[eval_type]
        eval_ap_results, eval_aos_results = {}, {}
        for cls in CLASSES:
            eval_ap_results[cls] = []
            eval_aos_results[cls] = []
            CLS_MIN_IOU = MIN_IOUS[cls][e_ind]
            for difficulty in [0, 1, 2]:
                # 1. bbox property
                total_gt_ignores, total_det_ignores, total_dc_bboxes, total_scores = [], [], [], []
                total_gt_alpha, total_det_alpha = [], []
                for id in ids:
                    gt_result = gt_results[id]['annos']
                    det_result = det_results[id]

                    # 1.1 gt bbox property
                    cur_gt_names = gt_result['name']
                    cur_difficulty = gt_result['difficulty']
                    cur_difficulty[:] = 1
                    gt_ignores, dc_bboxes = [], []
                    for j, cur_gt_name in enumerate(cur_gt_names):
                        # If not enough points in gt box, then ignore should also be true
                        ignore = cur_difficulty[j] < 0 or cur_difficulty[j] > difficulty or gt_result['num_points_in_gt'][j] < MIN_NUM_POINTS_THRESHOLD

                        if cur_gt_name == cls:
                            valid_class = 1
                        elif cls == 'Pedestrian' and cur_gt_name == 'Person_sitting':
                            valid_class = 0
                        elif cls == 'Car' and cur_gt_name == 'Van':
                            valid_class = 0
                        else:
                            valid_class = -1
                        
                        if valid_class == 1 and not ignore:
                            gt_ignores.append(0)
                        elif valid_class == 0 or (valid_class == 1 and ignore):
                            gt_ignores.append(1)
                        else:
                            gt_ignores.append(-1)
                        
                        if cur_gt_name == 'DontCare':
                            dc_bboxes.append(gt_result['bbox'][j])
                    total_gt_ignores.append(gt_ignores)
                    total_dc_bboxes.append(np.array(dc_bboxes))
                    total_gt_alpha.append(gt_result['alpha'])

                    # 1.2 det bbox property
                    cur_det_names = det_result['name']
                    cur_det_heights = det_result['bbox'].reshape(-1, 4)[:, 3] - det_result['bbox'].reshape(-1, 4)[:, 1]
                    det_ignores = []
                    for j, cur_det_name in enumerate(cur_det_names):
                        if cur_det_heights[j] < MIN_HEIGHT[difficulty]:
                            det_ignores.append(1)
                        elif cur_det_name == cls:
                            det_ignores.append(0)
                        else:
                            det_ignores.append(-1)
                    total_det_ignores.append(det_ignores)
                    total_scores.append(det_result['score'])
                    total_det_alpha.append(det_result['alpha'])

                # 2. calculate scores thresholds for PR curve
                tp_scores = []
                for i, id in enumerate(ids):
                    cur_eval_ious = eval_ious[i]
                    gt_ignores, det_ignores = total_gt_ignores[i], total_det_ignores[i]
                    scores = total_scores[i]

                    nn, mm = cur_eval_ious.shape
                    assigned = np.zeros((mm, ), dtype=np.bool_)
                    for j in range(nn):
                        if gt_ignores[j] == -1:
                            continue
                        match_id, match_score = -1, -1
                        for k in range(mm):
                            if not assigned[k] and det_ignores[k] >= 0 and cur_eval_ious[j, k] > CLS_MIN_IOU and scores[k] > match_score:
                                match_id = k
                                match_score = scores[k]
                        if match_id != -1:
                            assigned[match_id] = True
                            if det_ignores[match_id] == 0 and gt_ignores[j] == 0:
                                tp_scores.append(match_score)
                total_num_valid_gt = np.sum([np.sum(np.array(gt_ignores) == 0) for gt_ignores in total_gt_ignores])
                score_thresholds = get_score_thresholds(tp_scores, total_num_valid_gt)    
            
                # 3. draw PR curve and calculate mAP
                tps, fns, fps, total_aos = [], [], [], []

                for score_threshold in score_thresholds:
                    tp, fn, fp = 0, 0, 0
                    aos = 0
                    for i, id in enumerate(ids):
                        cur_eval_ious = eval_ious[i]
                        gt_ignores, det_ignores = total_gt_ignores[i], total_det_ignores[i]
                        gt_alpha, det_alpha = total_gt_alpha[i], total_det_alpha[i]
                        scores = total_scores[i]

                        nn, mm = cur_eval_ious.shape
                        assigned = np.zeros((mm, ), dtype=np.bool_)
                        for j in range(nn):
                            if gt_ignores[j] == -1:
                                continue
                            match_id, match_iou = -1, -1
                            for k in range(mm):
                                if not assigned[k] and det_ignores[k] >= 0 and scores[k] >= score_threshold and cur_eval_ious[j, k] > CLS_MIN_IOU:
    
                                    if det_ignores[k] == 0 and cur_eval_ious[j, k] > match_iou:
                                        match_iou = cur_eval_ious[j, k]
                                        match_id = k
                                    elif det_ignores[k] == 1 and match_iou == -1:
                                        match_id = k

                            if match_id != -1:
                                assigned[match_id] = True
                                if det_ignores[match_id] == 0 and gt_ignores[j] == 0:
                                    tp += 1
                                    if eval_type == 'bbox_2d':
                                        aos += (1 + np.cos(gt_alpha[j] - det_alpha[match_id])) / 2
                            else:
                                if gt_ignores[j] == 0:
                                    fn += 1
                            
                        for k in range(mm):
                            if det_ignores[k] == 0 and scores[k] >= score_threshold and not assigned[k]:
                                fp += 1
                        
                        # In case 2d bbox evaluation, we should consider dontcare bboxes
                        if eval_type == 'bbox_2d':
                            dc_bboxes = total_dc_bboxes[i]
                            det_bboxes = det_results[id]['bbox']
                            if len(dc_bboxes) > 0:
                                ious_dc_det = iou2d(torch.from_numpy(det_bboxes), torch.from_numpy(dc_bboxes), metric=1).numpy().T
                                for j in range(len(dc_bboxes)):
                                    for k in range(len(det_bboxes)):
                                        if det_ignores[k] == 0 and scores[k] >= score_threshold and not assigned[k]:
                                            if ious_dc_det[j, k] > CLS_MIN_IOU:
                                                fp -= 1
                                                assigned[k] = True
                            
                    tps.append(tp)
                    fns.append(fn)
                    fps.append(fp)
                    if eval_type == 'bbox_2d':
                        total_aos.append(aos)

                tps, fns, fps = np.array(tps), np.array(fns), np.array(fps)

                recalls = tps / (tps + fns)
                precisions = tps / (tps + fps)

                if eval_type == 'bbox_3d' and difficulty == 2 and cls == 'Car':
                    pr_file_path = os.path.join(saved_path, 'pr.txt')
                    write_out_prs(pr_file_path, precisions, recalls)
                
                for i in range(len(score_thresholds)):
                    precisions[i] = np.max(precisions[i:])

                sums_AP = 0
                for i in range(0, len(score_thresholds)):
                    sums_AP += precisions[i]
                mAP = sums_AP / 40 * 100
                eval_ap_results[cls].append(mAP)

                if eval_type == 'bbox_2d':
                    total_aos = np.array(total_aos)
                    similarity = total_aos / (tps + fps)
                    for i in range(len(score_thresholds)):
                        similarity[i] = np.max(similarity[i:])
                    sums_similarity = 0
                    for i in range(0, len(score_thresholds), 4):
                        sums_similarity += similarity[i]
                    mSimilarity = sums_similarity / 11 * 100
                    eval_aos_results[cls].append(mSimilarity)

        print(f'=========={eval_type.upper()}==========')
        print(f'=========={eval_type.upper()}==========', file=f)
        for k, v in eval_ap_results.items():
            print(f'{k} AP@{MIN_IOUS[k][e_ind]}: {v[0]:.4f} {v[1]:.4f} {v[2]:.4f}')
            print(f'{k} AP@{MIN_IOUS[k][e_ind]}: {v[0]:.4f} {v[1]:.4f} {v[2]:.4f}', file=f)
        if eval_type == 'bbox_2d':
            print(f'==========AOS==========')
            print(f'==========AOS==========', file=f)
            for k, v in eval_aos_results.items():
                print(f'{k} AOS@{MIN_IOUS[k][e_ind]}: {v[0]:.4f} {v[1]:.4f} {v[2]:.4f}')
                print(f'{k} AOS@{MIN_IOUS[k][e_ind]}: {v[0]:.4f} {v[1]:.4f} {v[2]:.4f}', file=f)
        
        overall_results[eval_type] = np.mean(list(eval_ap_results.values()), 0)
        if eval_type == 'bbox_2d':
            overall_results['AOS'] = np.mean(list(eval_aos_results.values()), 0)
    
    print(f'\n==========Overall==========')
    print(f'\n==========Overall==========', file=f)
    for k, v in overall_results.items():
        print(f'{k} AP: {v[0]:.4f} {v[1]:.4f} {v[2]:.4f}')
        print(f'{k} AP: {v[0]:.4f} {v[1]:.4f} {v[2]:.4f}', file=f)
    f.close()
    

def main(args):
    val_dataset = Kitti(data_root=args.data_root,
                        split='val')
    val_dataloader = get_dataloader(dataset=val_dataset, 
                                    batch_size=args.batch_size, 
                                    num_workers=args.num_workers,
                                    randomize=False)
    CLASSES = Kitti.CLASSES
    LABEL2CLASSES = {v:k for k, v in CLASSES.items()}

    if not args.no_cuda:
        model = PointPillars(nclasses=args.nclasses, points_bonus_dims=2).cuda()
        model.load_state_dict(torch.load(args.ckpt))
    else:
        model = PointPillars(nclasses=args.nclasses, points_bonus_dims=2)
        model.load_state_dict(
            torch.load(args.ckpt, map_location=torch.device('cpu')))
    
    saved_path = args.saved_path
    os.makedirs(saved_path, exist_ok=True)
    saved_submit_path = os.path.join(saved_path, 'submit')
    os.makedirs(saved_submit_path, exist_ok=True)

    pcd_limit_range = np.array([0, -40, -3, 70.4, 40, 0.0], dtype=np.float32)

    model.eval()
    with torch.no_grad():
        format_results = {}
        print('Predicting and Formatting the results.')
        for i, data_dict in enumerate(tqdm(val_dataloader)):
            if not args.no_cuda:
                # move the tensors to the cuda
                for key in data_dict:
                    for j, item in enumerate(data_dict[key]):
                        if torch.is_tensor(item):
                            data_dict[key][j] = data_dict[key][j].cuda()
            
            batched_pts = data_dict['batched_pts']
            batched_gt_bboxes = data_dict['batched_gt_bboxes']
            batched_labels = data_dict['batched_labels']
            batched_difficulty = data_dict['batched_difficulty']
            batch_results = model(batched_pts=batched_pts, 
                                  mode='val',
                                  batched_gt_bboxes=batched_gt_bboxes, 
                                  batched_gt_labels=batched_labels)

            # pdb.set_trace()
            for j, result in enumerate(batch_results):
                format_result = {
                    'name': [],
                    'truncated': [],
                    'occluded': [],
                    'alpha': [],
                    'bbox': [],
                    'dimensions': [],
                    'location': [],
                    'rotation_y': [],
                    'score': []
                }
                
                calib_info = data_dict['batched_calib_info'][j]
                tr_velo_to_cam = calib_info['Tr_velo_to_cam'].astype(np.float32)
                r0_rect = calib_info['R0_rect'].astype(np.float32)
                P2 = calib_info['P2'].astype(np.float32)
                image_shape = data_dict['batched_img_info'][j]['image_shape']
                idx = data_dict['batched_img_info'][j]['image_idx']
                result_filter = keep_bbox_from_image_range(result, tr_velo_to_cam, r0_rect, P2, image_shape)
                result_filter = keep_bbox_from_lidar_range(result_filter, pcd_limit_range)

                lidar_bboxes = result_filter['lidar_bboxes']
                labels, scores = result_filter['labels'], result_filter['scores']
                bboxes2d, camera_bboxes = result_filter['bboxes2d'], result_filter['camera_bboxes']
                # phony_data = [
                #     {
                #         'dimensions': [4.48, 1.71, 2.15],
                #         'location': [5.20091689666483, 0.63991689666, 11.78355167],
                #         'rotation_y': -3.10559
                #     },
                #     {
                #         'dimensions': [4.19, 1.45, 2.05],
                #         'location': [-3.90105, 0.7279488, 17.257584],
                #         'rotation_y': -3.08559
                #     }
                # ]

                for i, (lidar_bbox, label, score, bbox2d, camera_bbox) in \
                    enumerate(zip(lidar_bboxes, labels, scores, bboxes2d, camera_bboxes)):
                    format_result['name'].append(LABEL2CLASSES[label])
                    format_result['truncated'].append(0.0)
                    format_result['occluded'].append(0)
                    alpha = camera_bbox[6] - np.arctan2(camera_bbox[0], camera_bbox[2])
                    format_result['alpha'].append(alpha)
                    format_result['bbox'].append(bbox2d)
                    if False:
                        format_result['dimensions'].append(phony_data[i]['dimensions'])
                        format_result['location'].append(phony_data[i]['location'])
                        format_result['rotation_y'].append(phony_data[i]['rotation_y'])
                        format_result['score'].append(1.0)
                    else:
                        format_result['dimensions'].append(camera_bbox[3:6])
                        format_result['location'].append(camera_bbox[:3])
                        format_result['rotation_y'].append(camera_bbox[6])
                        format_result['score'].append(score)
                
                write_label(format_result, os.path.join(saved_submit_path, f'{idx}.txt'))

                format_results[idx] = {k:np.array(v) for k, v in format_result.items()}
        
        write_pickle(format_results, os.path.join(saved_path, 'results.pkl'))
    
    print('Evaluating.. Please wait several seconds.')
    if EVAL_TRACKING:
        do_tracking(format_results, CLASSES, saved_path)
    else:
        do_eval(format_results, val_dataset.data_infos, CLASSES, saved_path)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Configuration Parameters')
    parser.add_argument('--data_root', default='/mnt/ssd1/lifa_rdata/det/kitti', 
                        help='your data root for kitti')
    parser.add_argument('--ckpt', default='pretrained/epoch_160.pth', help='your checkpoint for kitti')
    parser.add_argument('--saved_path', default='results', help='your saved path for predicted results')
    parser.add_argument('--batch_size', type=int, default=1)
    parser.add_argument('--num_workers', type=int, default=4)
    parser.add_argument('--nclasses', type=int, default=3)
    parser.add_argument('--no_cuda', action='store_true',
                        help='whether to use cuda')
    args = parser.parse_args()

    main(args)
